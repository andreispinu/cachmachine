public class MessageMenu {
    public static void startMenu(){
        System.out.println(" _____________________________________________________");
        System.out.println("| 1<-Account information           Make an account->5 |");
        System.out.println("| 2<-Deposit                              Transfer->6 |");
        System.out.println("| 3<-PIN Change                         Withdrawal->7 |");
        System.out.println("| 4<-Balance                                  Exit->8 |");
        System.out.println(" ----------------------------------------------------- ");
        System.out.println("               Choose one of the options:              ");
    }
}

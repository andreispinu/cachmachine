import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static final Scanner scan = new Scanner(System.in);
    public static HashMap<String,String> dataPin = new HashMap<String, String>();
    public static HashMap<String,String> dataName = new HashMap<String, String>();
    public static HashMap<String,String> dataLastName = new HashMap<String, String>();
    public static HashMap<String,String> dataIBAN = new HashMap<String, String>();
    public static HashMap<String,Integer> dataBalance = new HashMap<String, Integer>();
    public static HashMap<String,Boolean> dataUsername = new HashMap<String, Boolean>();
    public static HashMap<String,String> dataCNP = new HashMap<String, String>();

    public static void main(String[] args){

        Person person = new Person();
        BankAccount bankAccount = new BankAccount();
        int choice;
        String pin;
        String username;
        String IBAN;
        do {
            MessageMenu.startMenu();
            choice = scan.nextInt();
            switch (choice){

                case 1 : {
                    System.out.print("Username: ");
                    username = scan.next();
                    System.out.print("PIN: ");
                    pin = scan.next();
                    while (dataPin.get(username)==null || !(dataPin.get(username).equals(pin)) || dataUsername.get(username)==null){
                        System.out.print("Invalid username or PIN. Try again.\nUsername: ");
                        username=scan.next();
                        System.out.print("PIN: ");
                        pin=scan.next();
                    }
                    person=switchAccount(username);
                    MenuOptions.accountInformation(person);
                    break;
                }

                case 2 :
                    System.out.print("Username: ");
                    username = scan.next();
                    System.out.print("PIN: ");
                    pin = scan.next();
                    while (dataPin.get(username)==null || !(dataPin.get(username).equals(pin)) || dataUsername.get(username)==null){
                        System.out.print("Invalid username or PIN. Try again.\nUsername: ");
                        username=scan.next();
                        System.out.print("PIN: ");
                        pin=scan.next();
                    }
                    System.out.println("Enter the money!");
                    int amount = scan.nextInt();
                    dataBalance.put(username,dataBalance.get(username)+amount);
                    System.out.println("Thank you!");
                    break;

                case 3 :
                    System.out.print("Username: ");
                    username = scan.next();
                    System.out.print("PIN: ");
                    pin = scan.next();
                    while (dataPin.get(username)==null || !(dataPin.get(username).equals(pin)) || dataUsername.get(username)==null){
                        System.out.print("Invalid username or PIN. Try again.\nUsername: ");
                        username=scan.next();
                        System.out.print("PIN: ");
                        pin=scan.next();
                    }
                    System.out.print("New pin:");
                    dataPin.put(username,MenuOptions.pinChange(username));
                    break;

                case 4 :
                    System.out.print("Username: ");
                    username = scan.next();
                    System.out.print("PIN: ");
                    pin = scan.next();
                    while (dataPin.get(username)==null || !(dataPin.get(username).equals(pin)) || dataUsername.get(username)==null){
                        System.out.print("Invalid username or PIN. Try again.\nUsername: ");
                        username=scan.next();
                        System.out.print("PIN: ");
                        pin=scan.next();
                    }

                    MenuOptions.balace(dataBalance.get(username));
                    break;

                case 5 :
                    MenuOptions.makeAnAccount(person, bankAccount);
                    dataName.put(bankAccount.username,person.name);
                    dataLastName.put(bankAccount.username,person.lastName);
                    dataPin.put(bankAccount.username, person.bankAccount.PIN);
                    dataIBAN.put(bankAccount.username,person.bankAccount.IBAN);
                    dataBalance.put(bankAccount.username,person.bankAccount.balance);
                    dataCNP.put(bankAccount.username, person.CNP);
                    dataUsername.put(bankAccount.username,true);
                    break;

                case 6 :
                    System.out.print("Username: ");
                    String username1 = scan.next();
                    System.out.print("PIN: ");
                    pin = scan.next();

                    while (dataPin.get(username1)==null || !(dataPin.get(username1).equals(pin)) || dataUsername.get(username1)==null){
                        System.out.print("Invalid username or PIN. Try again.\nUsername: ");
                        username1=scan.next();
                        System.out.print("PIN: ");
                        pin=scan.next();
                    }

                    System.out.println("Complete with details about the person to whom you are transferring.");

                    System.out.print("Username:");
                    username = scan.next();

                    System.out.print("IBAN:");
                    IBAN = scan.next();

                    while (!dataIBAN.containsValue(IBAN) || dataUsername.get(username)==null || !dataIBAN.get(username).equals(IBAN)){
                        System.out.println("Incorect details. Try again.");

                        System.out.print("Username:");
                        username = scan.next();

                        System.out.print("IBAN:");
                        IBAN = scan.next();
                    }

                    System.out.print("What amount do you transfer: ");
                    int newAmount = scan.nextInt();
                    while (newAmount>dataBalance.get(username1)){
                        System.out.println("Insufficient funds.\nTry again.");
                        System.out.print("What amount do you transfer: ");
                        newAmount = scan.nextInt();
                    }

                    dataBalance.put(username1,dataBalance.get(username1)-newAmount);
                    dataBalance.put(username, dataBalance.get(username)+newAmount);
                    System.out.println("Current balance: " + dataBalance.get(username1));
                    System.out.println("Thank you!");
                    break;

                case 7 :
                    System.out.print("Username: ");
                    username = scan.next();
                    System.out.print("PIN: ");
                    pin = scan.next();
                    while (dataPin.get(username)==null || !(dataPin.get(username).equals(pin)) || dataUsername.get(username)==null){
                        System.out.print("Invalid username or PIN. Try again.\nUsername: ");
                        username=scan.next();
                        System.out.print("PIN: ");
                        pin=scan.next();
                    }
                    System.out.print("What amount do you withdraw: ");
                    newAmount = scan.nextInt();
                    while (newAmount>dataBalance.get(username)){
                        System.out.println("Insufficient funds.\nTry again.");
                        System.out.print("What amount do you withdraw: ");
                        newAmount = scan.nextInt();
                    }
                    dataBalance.put(username,dataBalance.get(username)-newAmount);
                    System.out.println("Current balance: " + dataBalance.get(username));
                    System.out.println("Thank you!");
                    break;

                case 8 :
                    break;

                default :
                    System.out.println("Try again!");;
            }
        } while(choice!=8);

    }

    public static Person switchAccount(String username){
        Person person = new Person();
        BankAccount bankAccount = new BankAccount();
        person.name= dataName.get(username);
        person.lastName=dataLastName.get(username);
        person.CNP=dataCNP.get(username);
        bankAccount.username=username;
        bankAccount.IBAN=dataIBAN.get(username);
        bankAccount.PIN=dataPin.get(username);
        bankAccount.balance=dataBalance.get(username);
        person.bankAccount=bankAccount;
        return person;
    }
}
